import java.util.Scanner;

public class BoardGameApp{
	public static void main(String[] args){
		System.out.println("WELCOME TO SIMPLE WAR GAME");
		Scanner reader = new Scanner(System.in);
		Board game = new Board(5); //initializes a board of 5x5
		int numCastles = 7;
		int amountTurns = 8;
		int gameTurn = 1;
		
		
		while (amountTurns != 0 && numCastles != 0) {
			System.out.println("TURN " + gameTurn + ":");
			System.out.println("Number of Castles Left: " + numCastles);
			
			System.out.println(game);
			System.out.println("Column you'd like to place your token in:");
			int cols = Integer.parseInt(reader.nextLine());
			System.out.println("Row you'd like to place your token in:");
			int rows = Integer.parseInt(reader.nextLine());
			
			if (game.placeToken(rows-1, cols-1) == -2 || game.placeToken(rows-1, cols-1) == -1) {
				System.out.println("Invalid Input! TRY AGAIN");
				amountTurns++;
			}
			else if (game.placeToken(rows-1, cols-1) == 1) {
				System.out.println("There was a WALL!!");
			}
			else {
				numCastles--;
			}
				
			System.out.println(game);
			
			amountTurns--;
			gameTurn++;
		}
		
		if (numCastles >= 0){
			System.out.println("YOU WON");
		}
		else {
			System.out.println("GAME OVER!");
		}
	}
}