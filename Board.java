import java.util.Random;

public class Board{
	private Tile[][] grid;
	private int size; // For all different types of boards
	
	public Board(int size) {
		Random rng = new Random();
		this.size = size;
		this.grid = new Tile[size][size]; // makes an array of the size you choose
		for (int i = 0; i< this.grid.length; i++) {
			for (int j = 0; j< this.grid[i].length; j++) {
				this.grid[i][j] = Tile.BLANK;
			}
			grid[i][rng.nextInt(grid[i].length)] = Tile.HIDDEN_WALL; // makes a hidden wall on a random spot in a row (once every row)
		}
	}
	
	public String toString(){
		String output = "";
		for (int i = 0; i < this.grid.length; i++) {
			for (int j = 0; j < this.grid.length; j++) {
				output += this.grid[i][j].getName() + " ";
			}
			output += "\n";
		}
		return output;
	}
	
	public int placeToken(int row, int col) {
		if (row < 0 || row >= size || col < 0 || col >= size) { //if column or row is higher than the board size or is lower than the minimum(0)
			return -2;
		}
		else if (this.grid[row][col] == Tile.CASTLE || this.grid[row][col] == Tile.WALL) {
			return -1;
		}
		else if (this.grid[row][col] == Tile.HIDDEN_WALL) {
			this.grid[row][col] = Tile.WALL;
			return 1;
		}
		else {
			this.grid[row][col] = Tile.CASTLE;
			return 0;
		}
	}
	
	
}